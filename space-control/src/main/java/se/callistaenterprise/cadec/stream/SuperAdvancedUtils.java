package se.callistaenterprise.cadec.stream;

import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import se.callistaenterprise.cadec.stream.model.Meteor;
import se.callistaenterprise.cadec.stream.model.Ship;

import java.awt.*;

@Slf4j
public class SuperAdvancedUtils {

    public static se.callistaenterprise.cadec.stream.model.Event collide(final Ship ship, final Meteor meteor) {
        if(ship == null) {
            return se.callistaenterprise.cadec.stream.model.Event.newBuilder().setShipId(0).setMeteorId(0).setCrashed(false).build();
        }
        final Rectangle shipRectangle = new Rectangle(p(ship.getX(), ship.getY()), d(ship.getW(), ship.getH()));
        final Rectangle meteorRectangle = new Rectangle(p(meteor.getX(), meteor.getY()), d(meteor.getW(), meteor.getH()));

        final boolean intersects = shipRectangle.intersects(meteorRectangle);

        if(intersects) {
            log.info("-----------------------------------");
            log.info("M: {}", meteor.toString());
            log.info("S: {}", ship.toString());
            log.info("-----------------------------------");
        }

        return se.callistaenterprise.cadec.stream.model.Event.newBuilder()
                .setShipId(ship.getId())
                .setMeteorId(meteor.getId())
                .setCrashed(intersects).build();
    }

    private static Dimension d(final int w, final int h) {
        return new Dimension(w,h);
    }

    private static Point p(final int x, final int y) {
        return new Point(x,y);
    }
}
