package se.callistaenterprise.cadec.stream;

import io.confluent.kafka.serializers.AbstractKafkaAvroSerDeConfig;
import io.confluent.kafka.streams.serdes.avro.SpecificAvroSerde;
import org.apache.kafka.clients.admin.AdminClientConfig;
import org.apache.kafka.clients.admin.NewTopic;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.processor.WallclockTimestampExtractor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.annotation.KafkaStreamsDefaultConfiguration;
import org.springframework.kafka.core.KafkaAdmin;
import se.callistaenterprise.cadec.stream.model.Event;
import se.callistaenterprise.cadec.stream.model.Meteor;
import se.callistaenterprise.cadec.stream.model.Ship;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

@Configuration
public class Config {

    @Value("${kafka.brokers}")
    private String kafkaBrokers;

    @Value("${kafka.schema_registry}")
    private String schemaRegistryUrl;

    @Bean(name = KafkaStreamsDefaultConfiguration.DEFAULT_STREAMS_CONFIG_BEAN_NAME)
    public StreamsConfig streamsConfigs() {
        Map<String, Object> props = new HashMap<>();
        props.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, kafkaBrokers);
        props.put(StreamsConfig.APPLICATION_ID_CONFIG, "spaceControl");
        props.put(StreamsConfig.CLIENT_ID_CONFIG, "spaceControlClient");
        props.put(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG, Serdes.ByteArray().getClass().getName());
        props.put(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG, Serdes.ByteArray().getClass().getName());
        props.put(StreamsConfig.TIMESTAMP_EXTRACTOR_CLASS_CONFIG, WallclockTimestampExtractor.class.getName());
        props.put(AbstractKafkaAvroSerDeConfig.SCHEMA_REGISTRY_URL_CONFIG, schemaRegistryUrl);
        props.put(StreamsConfig.CACHE_MAX_BYTES_BUFFERING_CONFIG, "0");
        props.put(StreamsConfig.COMMIT_INTERVAL_MS_CONFIG, "1");
        props.put(StreamsConfig.NUM_STREAM_THREADS_CONFIG, "2");
        props.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "latest");
        return new StreamsConfig(props);
    }

    @Bean("shipSerde")
    public SpecificAvroSerde<Ship> shipSerde() {
        final SpecificAvroSerde<Ship> shipSerde = new SpecificAvroSerde<>();
        shipSerde.configure(Collections.singletonMap(AbstractKafkaAvroSerDeConfig.SCHEMA_REGISTRY_URL_CONFIG, schemaRegistryUrl),
                false);
        return shipSerde;
    }

    @Bean("eventSerde")
    public SpecificAvroSerde<Event> eventSerde() {
        final SpecificAvroSerde<Event> eventSerde = new SpecificAvroSerde<>();
        eventSerde.configure(Collections.singletonMap(AbstractKafkaAvroSerDeConfig.SCHEMA_REGISTRY_URL_CONFIG, schemaRegistryUrl),
                false);
        return eventSerde;
    }

    @Bean("meteorSerde")
    public SpecificAvroSerde<Meteor> meteorSerde() {
        final SpecificAvroSerde<Meteor> meteorSerde = new SpecificAvroSerde<>();
        meteorSerde.configure(Collections.singletonMap(AbstractKafkaAvroSerDeConfig.SCHEMA_REGISTRY_URL_CONFIG, schemaRegistryUrl),
                false);
        return meteorSerde;
    }

}
