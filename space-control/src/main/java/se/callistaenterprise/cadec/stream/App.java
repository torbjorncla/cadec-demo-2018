package se.callistaenterprise.cadec.stream;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.kafka.annotation.EnableKafka;
import org.springframework.kafka.annotation.EnableKafkaStreams;

@SpringBootApplication
@EnableAutoConfiguration
@EnableKafka
@EnableKafkaStreams
public class App {
    public static void main(String... args) {
        SpringApplication.run(App.class, args);
    }



}
