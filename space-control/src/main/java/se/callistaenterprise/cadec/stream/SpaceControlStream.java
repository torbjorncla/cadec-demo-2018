package se.callistaenterprise.cadec.stream;

import com.google.gson.internal.Streams;
import io.confluent.kafka.streams.serdes.avro.SpecificAvroSerde;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.Consumed;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.kstream.KStream;
import org.apache.kafka.streams.kstream.KTable;
import org.apache.kafka.streams.kstream.Materialized;
import org.apache.kafka.streams.kstream.Produced;
import org.apache.kafka.streams.state.Stores;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;
import se.callistaenterprise.cadec.stream.model.Event;
import se.callistaenterprise.cadec.stream.model.Meteor;
import se.callistaenterprise.cadec.stream.model.Ship;


@Component
@Slf4j
public class SpaceControlStream {

    @Autowired
    @Qualifier("shipSerde")
    private SpecificAvroSerde<Ship> shipSerde;

    @Autowired
    @Qualifier("meteorSerde")
    private SpecificAvroSerde<Meteor> meteorSerde;

    @Autowired
    @Qualifier("eventSerde")
    private SpecificAvroSerde<Event> eventSerde;


    @Bean
    public KStream<String, Meteor> meteorKStream(final StreamsBuilder builder) {
        final KTable<String, Ship> shipKTable = builder.table("Ship",
                Consumed.with(Serdes.String(), shipSerde),
                Materialized.as(Stores.inMemoryKeyValueStore("shipStore")));

        final KStream<String, Meteor> meteorKStream = builder.stream("Meteor",
                Consumed.with(Serdes.String(), meteorSerde));

        meteorKStream.leftJoin(shipKTable, (meteor, ship) -> SuperAdvancedUtils.collide(ship, meteor))
        .filter((zone, event) -> event.getCrashed())
        .to("Event", Produced.with(Serdes.String(), eventSerde));

        return meteorKStream;
    }
}
