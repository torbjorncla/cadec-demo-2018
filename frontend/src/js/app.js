window.onload = function() {

    var c = false;
    var d = false;
    var o = false;
    var draw_once = true

    var canvas = {
        w: window.innerWidth,
        h: window.innerHeight,
        xMax: window.innerWidth,
        xMin: 0,
        yMax: window.innerHeight,
        yMin: 0,
    };

    var meteorSocket = new WebSocket("ws://localhost:8090/meteor");
    var meteorSocketOnline = false;
    var shipSocket = new WebSocket("ws://localhost:8090/ship");
    var shipSocketOnline = false;
    var eventSocket = new WebSocket("ws://localhost:8090/event")


    shipSocket.onopen = function() {
        var clear_ship = {"id": 1, "x": 1, "y": 1, "w": 1, "h": 1, "bear": 1, "zone": "A", "delete": true}
            shipSocket.send(JSON.stringify(clear_ship));
            clear_ship.zone = "B"
            shipSocket.send(JSON.stringify(clear_ship));
            clear_ship.zone = "C"
            shipSocket.send(JSON.stringify(clear_ship));
            clear_ship.zone = "D"
            shipSocket.send(JSON.stringify(clear_ship));
            shipSocketOnline = true;
            console.log("Onopen");
    }

    shipSocket.onmessage = function(data) {
        //console.log(data)
    }

    shipSocket.onclose = function(e) {
        console.log(e)
        shipSocketOnline = false
    }

    meteorSocket.onopen = function() {
        meteorSocketOnline = true;
    }

    eventSocket.onmessage = function(e) {
        var event = JSON.parse(e.data)
        if(event.shipId == 1) {
            exp(null, ship1)
        }
        if(event.shipId == 2) {
            exp(null, ship2);
        }
    }

    var meteor_img = ["meteor_grey", "meteor_grey", "meteor_brown", "meteor_brown", "meteor_brown"]

    Meteor = function(game, ghost, id, l1, l2, l3, l4) {
        Phaser.Sprite.call(this, game, game.world.randomX, game.world.randomY, meteor_img[game.rnd.integerInRange(0,4)])

        this.anchor.setTo(0.5, 0.5)
        this.id = id
        this.scale.set(game.rnd.realInRange(1, 0.6))
        this.rotateSpeed = game.rnd.realInRange(0.5, 2)
        this.speed = game.rnd.realInRange(60, 90)
        this.targetX = game.world.randomX
        this.targetY = game.world.randomY
        this.ghost = ghost
        this.posTicker = 60;
        this.zone = ""

                this.json = {
                    "zone": this.zone,
                    "type": 3,
                    "id": id,
                    "x": 0,
                    "y": 0,
                    "w": 0,
                    "h": 0,
                    "bear": 0
                }

         this.enableBody = true;
         this.physicsBodyType = Phaser.Physics.ARCADE;

        this.animations.add('kaboom');

        this.landings = [l1, l2, l3, l4]
        game.physics.arcade.enable(this)
        game.add.existing(this)
    }

    Meteor.prototype = Object.create(Phaser.Sprite.prototype)
    Meteor.prototype.constructor = Meteor

    Meteor.prototype.update = function() {
        self = this

        this.angle += this.rotateSpeed

        this.json.x = Math.floor(this.position.x - (this.height / 2));
                this.json.y = Math.floor(this.position.y - (this.width / 2));

                this.json.h = Math.floor(this.height);
                this.json.w = Math.floor(this.width);

        var self = this
            if(offGrid(this)) {
                this.targetX = game.world.randomX
                this.targetY = game.world.randomY
                game.physics.arcade.moveToXY(this, this.targetX, this.targetY, this.speed, 0);
            }
            this.landings.forEach(function(l) {
                if(game.physics.arcade.collide(self, l)) {
                    this.targetX = game.world.randomX
                    this.targetY = game.world.randomY
                    game.physics.arcade.moveToXY(self, this.targetX, this.targetY, this.speed, 0);
                }
            })
            meteors.forEach(function(m) {
                if(self.id != m.id) {
                    if(game.physics.arcade.collide(self, m)) {
                        this.targetX = game.world.randomX
                        this.targetY = game.world.randomY
                        game.physics.arcade.moveToXY(self, this.targetX, this.targetY, this.speed, 0);
                    }
                }
            });

            setZone(this, false)
    }

    Landing = function(game, x, y) {
        Phaser.Sprite.call(this, game, x, y, "landing");
        this.scale.set(1.4)
        game.physics.arcade.enable(this)

        this.body.immovable = true;

        game.add.existing(this)
    }

    Landing.prototype = Object.create(Phaser.Sprite.prototype)
    Landing.prototype.constructor = Landing

    Landing.prototype.update = function() {
    }

    Ship = function(game, id, ghost, img, angle,a1, a2, speed, startLanding, stopLanding) {
        Phaser.Sprite.call(this, game, startLanding.position.x, startLanding.position.y, img)
        this.anchor.setTo(a1, a2)
        this.scale.set(0.6)
        this.angle = angle
        this.speed = 100;
        this.startLanding = startLanding
        this.stopLanding = stopLanding
        this.ghost = ghost
        this.hit = false
        this.zone = ""

        

                this.json = {
                    "zone": this.zone,
                  "type": 4,
                  "id": id,
                  "x": 0,
                  "y": 0,
                  "w": 0,
                  "h": 0,
                  "bear": angle,
                  "delete": false
                }

        this.enableBody = true;
        this.physicsBodyType = Phaser.Physics.ARCADE;

        this.animations.add("kaboom");

        game.physics.arcade.enable(this)
        game.add.existing(this)
    }

    Ship.prototype = Object.create(Phaser.Sprite.prototype)
    Ship.prototype.constructor = Ship

    Ship.prototype.update = function() {
        var x1 = this.stopLanding.position.x;
        var x2 = this.position.x;
        var y1 = this.stopLanding.position.y;
        var y2 = this.position.y;


        setZone(this, true)

        var self = this

        if(this.id == 1) {
            ship1Ghost.angle = this.angle
        }
        if(this.id == 2) {
            ship2Ghost.angle = this.angle
        }

        this.json.x = Math.floor(this.position.x);
                this.json.y = Math.floor(this.position.y);

                //Switch H with W, better hitbox?
                this.json.w = Math.floor(this.width);
                this.json.h = Math.floor(this.height);

        this.bear = this.angle

        var self = this
        if(game.math.distance(x1,y1,x2,y2) < 10) {
            //switch pos
            var back = self.stopLanding;
            self.stopLanding = self.startLanding
            self.startLanding = back;
            game.physics.arcade.moveToXY(self, self.stopLanding.position.x, self.stopLanding.position.y, self.speed, 0);

            if(self.angle == 90) {
                self.anchor.setTo(1,0)
                self.angle = 270
            } else {
                self.anchor.setTo(0,1)
                self.angle = 90
            }
        }

        
    }

    var game = new Phaser.Game(canvas.w, canvas.h, Phaser.AUTO, '', {
        preload: preload,
        create: create,
        update: update
    });

    function preload() {
        game.load.image("meteor_brown", "img/meteorBrown_big2.png")
        game.load.image("meteor_grey", "img/meteorGrey_big1.png")
        game.load.image("background", "img/back.png")
        game.load.image("sputnik", "img/spaceBuilding_014.png")
        game.load.image("landing", "img/spaceBuilding_025.png");
        game.load.image("ship_blue", "img/playerShip1_blue.png");
        game.load.image("ship_green", "img/playerShip1_green.png");
        game.load.spritesheet("kaboom", 'img/explode.png', 128, 128, 16);
        game.load.image("font", "img/AeroFont.png")
    }

    var landing1_1 = {}
    var landing1_2 = {}
    var landing2_1 = {}
    var landing2_2 = {}

    var meteors = []

    var ship1 = {}
    var ship2 = {}

    var bound1 = {}
    var bound2 = {}
    var bound3 = {}
    var bound4 = {}

    var rect = {}

    var explosions = {}

    var ck = {}
    var dk = {}

    var font;

    var zones = {}

    function create() {
        game.add.tileSprite(0, 0, canvas.xMax, canvas.yMax, "background");
        game.physics.startSystem(Phaser.Physics.ARCADE);
        game.world.setBounds(0, 0, canvas.w, canvas.h);
        game.stage.backgroundColor = "#00000"
        dk = game.input.keyboard.addKey(Phaser.Keyboard.D)
        dk.onDown.add(function() {
            d = true
        })

        var ay = Math.floor((canvas.yMax / 2) / 2);
        var by = canvas.yMax - ay


        zones["A"] = new Phaser.Rectangle(0, 0, canvas.xMax/2, canvas.yMax/2);
        zones["B"] = new Phaser.Rectangle(canvas.xMax/2, 0, canvas.xMax/2, canvas.yMax/2)
        zones["C"] = new Phaser.Rectangle(0, canvas.yMax/2, canvas.xMax/2, canvas.yMax/2)
        zones["D"] = new Phaser.Rectangle(canvas.xMax/2, canvas.yMax/2 ,canvas.xMax/2, canvas.yMax/2)

        landing1_1 = new Landing(game, 40, ay);
        landing2_1 = new Landing(game, 40, by);
        landing1_2 = new Landing(game, canvas.xMax - 120, ay)
        landing2_2 = new Landing(game, canvas.xMax - 120, by)

        for(i = 0; i < 8; i++) {
            var m = new Meteor(game, false, i, landing1_1, landing1_2, landing2_1, landing2_2)
            game.physics.arcade.moveToXY(m, m.targetX, m.targetY, m.speed, 0)
            meteors.push(m)
        }

        ship1 = new Ship(game, 1, false, "ship_blue", 90, 0, 1, 120, landing1_1, landing1_2);
        game.physics.arcade.moveToXY(ship1, landing1_2.position.x, landing1_2.position.y, ship1.speed, 0);

        ship2 = new Ship(game, 2, false, "ship_green", 270, 1, 0, 120, landing2_2, landing2_1);
        game.physics.arcade.moveToXY(ship2, landing2_1.position.x, landing2_1.position.y, ship2.speed, 0);

        explosions = game.add.group();
        explosions.createMultiple(30, "kaboom");
        for (var i = 0; i < explosions.children.length; i++) {
            explosions.children[i].animations.add("akaboom");
        };

        game.time.events.loop(200, pushData, this)


        font = game.add.retroFont("font", 16, 16, Phaser.RetroFont.TEXT_SET1)
        var font_img = game.add.image(game.world.centerX, game.world.centerY, font)
        font_img.scale.set(2);
        font_img.anchor.set(0.5);
        font_img.smoothed = false;
    }

    function pushData() {
        if(shipSocketOnline && meteorSocketOnline) {
            if(ship1 != null) {
                shipSocket.send(JSON.stringify(ship1.json));
            }
            if(ship2 != null) {
                shipSocket.send(JSON.stringify(ship2.json));
            }

            meteors.forEach(function(m) {
              meteorSocket.send(JSON.stringify(m.json))
            });
        }
    }
    

    function setZone(obj, isShip) {
       var preZone  = obj.zone
       if(zones["A"].intersects(obj)) {
           obj.zone = "A";
           obj.json.zone = "A"
       }
       if(zones["B"].intersects(obj)) {
           obj.zone = "B"
           obj.json.zone = "B"
       }
       if(zones["C"].intersects(obj)) {
           obj.zone = "C"
           obj.json.zone = "C"
       }
       if(zones["D"].intersects(obj)) {
            obj.zone = "D"
            obj.json.zone = "D"
       }
       if(preZone != obj.zone && isShip && shipSocketOnline) {
            var clear_ship = {"id": obj.id, "x": 1, "y": 1, "w": 1, "h": 1, "bear": 1, "zone": preZone, "delete": true}
            shipSocket.send(JSON.stringify(clear_ship));
       }
    }

    function update() {

        if(ship1.alive == false && ship2.alive == false && draw_once == true) {
            font.text = "Thank you for listening!"
            draw_once = false
        }

        if(d == true) {
            game.debug.geom(new Phaser.Line(0, canvas.yMax/2, canvas.xMax, canvas.yMax/2), "#ff0000");
            game.debug.geom(new Phaser.Line(canvas.xMax/2, 0, canvas.xMax/2, canvas.yMax), "#ff0000");
            game.debug.text("A", canvas.xMax/4, canvas.yMax/4);
            game.debug.text("B", canvas.xMax/2 + (canvas.xMax/4), canvas.yMax/4);
            game.debug.text("C", canvas.xMax/4, canvas.yMax/2 + (canvas.yMax/4));
            game.debug.text("D", canvas.xMax/2 + (canvas.xMax/4), canvas.yMax/2 + (canvas.yMax/4))
        }

    }

    function exp(m,ship) {
        if(ship.alive) {
        ship.hit = true
        var explosion = explosions.getFirstExists(false);
        explosion.reset(ship.body.x, ship.body.y);
        explosion.play('akaboom', 30, false, true);
        ship.kill()
        }
    }

    function offGrid(s) {
        var x = s.position.x
        var y = s.position.y
        if(x > canvas.xMax || canvas.xMin > x || y > canvas.yMax || canvas.yMin > y) {
            return true;
        } else {
            return false;
        }
    }
}
