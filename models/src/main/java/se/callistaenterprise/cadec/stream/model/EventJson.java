package se.callistaenterprise.cadec.stream.model;

public class EventJson extends Base {

    private final long shipId;
    private final long meteorId;
    private final boolean crashed;

    public EventJson(final String zone, long shipId, long meteorId, boolean crashed) {
        super(2, zone);
        this.shipId = shipId;
        this.meteorId = meteorId;
        this.crashed = crashed;
    }

    public boolean getCrashed() {
        return crashed;
    }

    public long getShipId() {
        return shipId;
    }

    public long getMeteorId() {
        return meteorId;
    }
}
