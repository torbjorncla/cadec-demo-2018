package se.callistaenterprise.cadec.stream.model;

public class Base {
    private final int type;
    private final String zone;

    public Base(final int type, final String zone) {
        this.type = type;
        this.zone = zone;
    }

    public int getType() {
        return type;
    }

    public String getZone() { return zone; }
}
