package se.callistaenterprise.cadec.stream.model;

public class ShipJson extends Base {
    private final long id;
    private final int x;
    private final int y;
    private final int h;
    private final int w;
    private final int bear;
    private final boolean delete;

    public ShipJson(final String zone, long id, int x, int y, int h, int w, int bear, boolean delete) {
        super(4, zone);
        this.id = id;
        this.x = x;
        this.y = y;
        this.h = h;
        this.w = w;
        this.bear = bear;
        this.delete = delete;
    }

    public long getId() {
        return id;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getH() {return h;}
    public int getW() {return w;}

    public int getBear() {return bear;}

    public boolean getDelete() {
        return delete;
    }
}
