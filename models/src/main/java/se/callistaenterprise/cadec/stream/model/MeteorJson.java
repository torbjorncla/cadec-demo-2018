package se.callistaenterprise.cadec.stream.model;

public class MeteorJson extends Base {
    private final long id;
    private final int x;
    private final int y;
    private final int h;
    private final int w;
    private final int bear;

    public MeteorJson(final String zone, long id, int x, int y, int h, int w, int bear) {
        super(3, zone);
        this.id = id;
        this.x = x;
        this.y = y;
        this.w = w;
        this.h = h;
        this.bear = bear;
    }

    public long getId() {
        return id;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getH() { return h;}
    public int getW() {return w;}

    public int getBear() {
        return bear;
    }
}
