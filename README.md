# Cadec Demo 2018

## Setup
$ cadec_streams/> gradle bootJar

$ cadec_streams/frontend/> docker build -t frontend .

$ cadec_streams/space-control/> docker build -t space_control .

$ cadec_streams/websocket-connect/> docker build -t websocket_connect .

$ cadec_streams/> docker-compose up -d


Browse to http://localhost:1338

## Issues
Since no health-checks exists you might need to restart docker-compose before running
since topics need to be created before starting applications.

