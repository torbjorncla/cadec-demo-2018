package se.callistaenterprise.cadec.stream;

import io.confluent.kafka.serializers.AbstractKafkaAvroSerDeConfig;
import io.confluent.kafka.streams.serdes.avro.SpecificAvroSerde;
import org.apache.kafka.clients.admin.AdminClientConfig;
import org.apache.kafka.clients.admin.NewTopic;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.serialization.ByteArraySerializer;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.StreamsConfig;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.annotation.KafkaStreamsDefaultConfiguration;
import org.springframework.kafka.core.DefaultKafkaProducerFactory;
import org.springframework.kafka.core.KafkaAdmin;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.core.ProducerFactory;
import org.springframework.web.reactive.socket.server.support.WebSocketHandlerAdapter;
import reactor.core.publisher.EmitterProcessor;
import se.callistaenterprise.cadec.stream.model.*;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

@Configuration
public class Config {

    @Bean
    public WebSocketHandlerAdapter handlerAdapter() {
        return new WebSocketHandlerAdapter();
    }

    @Bean
    public KafkaAdmin admin(@Value("${kafka.brokers}") String bootStrapServers) {
        final Map<String, Object> configs = new HashMap<>();
        configs.put(AdminClientConfig.BOOTSTRAP_SERVERS_CONFIG, bootStrapServers);
        return new KafkaAdmin(configs);
    }

    @Bean(name = KafkaStreamsDefaultConfiguration.DEFAULT_STREAMS_CONFIG_BEAN_NAME)
    public StreamsConfig streamsConfig(@Value("${kafka.brokers}") String bootStrapServers) {
        Map<String, Object> props = new HashMap<>();
        props.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, bootStrapServers);
        props.put(StreamsConfig.APPLICATION_ID_CONFIG, "websocketConnect");
        props.put(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG, Serdes.ByteArray().getClass().getName());
        props.put(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG, Serdes.ByteArray().getClass().getName());
        props.put(StreamsConfig.CACHE_MAX_BYTES_BUFFERING_CONFIG, "0");
        props.put(StreamsConfig.COMMIT_INTERVAL_MS_CONFIG, "1");
        props.put(StreamsConfig.NUM_STREAM_THREADS_CONFIG, String.valueOf(Runtime.getRuntime().availableProcessors()));
        return new StreamsConfig(props);
    }

    @Bean
    public ProducerFactory<byte[], byte[]> producerFactory(@Value("${kafka.brokers}") String bootStrapServers) {
        final Map<String, Object> config = new HashMap<>();
        config.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, bootStrapServers);
        config.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, ByteArraySerializer.class);
        config.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, ByteArraySerializer.class);
        return new DefaultKafkaProducerFactory<>(config);
    }

    @Bean
    public KafkaTemplate<byte[], byte[]> kafkaTemplate(ProducerFactory<byte[], byte[]> producerFactory) {
        return new KafkaTemplate<>(producerFactory, true);
    }


    @Bean("shipSerde")
    public SpecificAvroSerde<Ship> shipSerde(@Value("${kafka.schema_registry}") String schemaRegistryUrl) {
        final SpecificAvroSerde<Ship> shipSerde = new SpecificAvroSerde<>();
        shipSerde.configure(Collections.singletonMap(AbstractKafkaAvroSerDeConfig.SCHEMA_REGISTRY_URL_CONFIG, schemaRegistryUrl),
                false);
        return shipSerde;
    }

    @Bean("eventSerde")
    public SpecificAvroSerde<Event> eventSerde(@Value("${kafka.schema_registry}") String schemaRegistryUrl) {
        final SpecificAvroSerde<Event> eventSerde = new SpecificAvroSerde<>();
        eventSerde.configure(Collections.singletonMap(AbstractKafkaAvroSerDeConfig.SCHEMA_REGISTRY_URL_CONFIG, schemaRegistryUrl),
                false);
        return eventSerde;
    }

    @Bean("meteorSerde")
    public SpecificAvroSerde<Meteor> meteorSerde(@Value("${kafka.schema_registry}") String schemaRegistryUrl) {
        final SpecificAvroSerde<Meteor> meteorSerde = new SpecificAvroSerde<>();
        meteorSerde.configure(Collections.singletonMap(AbstractKafkaAvroSerDeConfig.SCHEMA_REGISTRY_URL_CONFIG, schemaRegistryUrl),
                false);
        return meteorSerde;
    }


    @Bean
    public EmitterProcessor<EventJson> eventProcessor() {
        return EmitterProcessor.create(false);
    }

    @Bean
    public EmitterProcessor<MeteorJson> meteorProcessor() {
        return EmitterProcessor.create(false);
    }

    @Bean
    public EmitterProcessor<ShipJson> shipProcessor() {
        return EmitterProcessor.create(false);
    }

    @Bean
    public NewTopic meteorTopic() {
        return new NewTopic("Meteor", 4, (short) 1);
    }

    @Bean
    public NewTopic shipTopic() {
        return new NewTopic("Ship", 4, (short) 1);
    }

    @Bean
    public NewTopic eventTopic() {
        return new NewTopic("Event", 4, (short) 1);
    }

}
