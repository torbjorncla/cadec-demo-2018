package se.callistaenterprise.cadec.stream;

import io.confluent.kafka.streams.serdes.avro.SpecificAvroSerde;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.Consumed;
import org.apache.kafka.streams.KeyValue;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.kstream.KStream;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;
import reactor.core.publisher.EmitterProcessor;
import se.callistaenterprise.cadec.stream.model.*;

import java.util.Objects;

@Component
@Slf4j
public class Streams {

    @Autowired
    private EmitterProcessor<EventJson> eventProcessor;

    @Autowired
    public EmitterProcessor<MeteorJson> meteorProcessor;

    @Autowired
    public EmitterProcessor<ShipJson> shipProcessor;

    @Autowired
    @Qualifier("shipSerde")
    private SpecificAvroSerde<Ship> shipSerde;

    @Autowired
    @Qualifier("meteorSerde")
    private SpecificAvroSerde<Meteor> meteorSerde;

    @Autowired
    @Qualifier("eventSerde")
    private SpecificAvroSerde<Event> eventSerde;


    @Bean
    public KStream<String, Event> eventStream(final StreamsBuilder builder) {
        final KStream<String, Event> stream = builder.stream("Event", Consumed.with(Serdes.String(), eventSerde));

        stream.map((zone, e) -> {
            return KeyValue.pair(zone, new EventJson(zone, e.getShipId(), e.getMeteorId(), e.getCrashed()));
        }).foreach((zone, event) -> {
            eventProcessor.onNext(event);
        });

        return stream;
    }

    @Bean
    public KStream<String, Meteor> meteorStream(final StreamsBuilder builder) {
        final KStream<String, Meteor> stream = builder.stream("Meteor", Consumed.with(Serdes.String(), meteorSerde));

        stream.map((zone, m) -> {
            return KeyValue.pair(zone, new MeteorJson(zone, m.getId(), m.getX(), m.getY(), m.getH(), m.getW(), m.getBear()));
        }).foreach((zone, m) -> {
            meteorProcessor.onNext(m);
        });

        return stream;
    }

    @Bean
    public KStream<String, Ship> shipKStream(final StreamsBuilder builder) {
        final KStream<String, Ship> stream = builder.stream("Ship", Consumed.with(Serdes.String(), shipSerde));

        stream.filter((zone, s) -> {
            return Objects.nonNull(s);
        }).map((zone, s) -> {
            return KeyValue.pair(zone, new ShipJson(zone, s.getId(), s.getX(), s.getY(), s.getH(), s.getW(), s.getBear(), false));
        }).foreach((zone, s) -> {
            log.info("Published ship event to ship-processor");
            shipProcessor.onNext(s);
        });

        return stream;
    }

}
