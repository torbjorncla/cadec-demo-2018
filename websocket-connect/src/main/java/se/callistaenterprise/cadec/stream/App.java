package se.callistaenterprise.cadec.stream;

import com.google.gson.Gson;
import io.confluent.kafka.streams.serdes.avro.SpecificAvroSerde;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.common.serialization.Serializer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.kafka.annotation.EnableKafka;
import org.springframework.kafka.annotation.EnableKafkaStreams;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.web.reactive.HandlerMapping;
import org.springframework.web.reactive.handler.SimpleUrlHandlerMapping;
import org.springframework.web.reactive.socket.WebSocketHandler;
import org.springframework.web.reactive.socket.WebSocketMessage;
import reactor.core.publisher.*;
import se.callistaenterprise.cadec.stream.model.*;

import java.util.HashMap;
import java.util.Map;

@Slf4j
@SpringBootApplication
@EnableAutoConfiguration
@EnableKafka
@EnableKafkaStreams
public class App {

    @Autowired
    private KafkaTemplate<byte[], byte[]> kafkaTemplate;

    @Autowired
    @Qualifier("shipSerde")
    private SpecificAvroSerde<Ship> shipSerde;

    @Autowired
    @Qualifier("meteorSerde")
    private SpecificAvroSerde<Meteor> meteorSerde;

    @Autowired
    @Qualifier("eventSerde")
    private SpecificAvroSerde<Event> eventSerde;

    @Autowired
    private EmitterProcessor<EventJson> eventProcessor;

    @Autowired
    public EmitterProcessor<MeteorJson> meteorProcessor;

    @Autowired
    public EmitterProcessor<ShipJson> shipProcessor;


    private final Gson gson = new Gson();

    public static void main(String... args) {
        SpringApplication.run(App.class, args);
    }

    @Bean
    public HandlerMapping socketMapping() {
        final Map<String, WebSocketHandler> map = new HashMap<>();

        map.put("/event", session -> session.send(eventProcessor.map(e -> gson.toJson(e)).map(session::textMessage)));


        map.put("/meteor", session -> {
            session.receive()
                    .map(WebSocketMessage::getPayloadAsText)
                    .map(json -> gson.fromJson(json, MeteorJson.class))
                    .subscribe(this::onMeteor,this::onError);
            return session.send(meteorProcessor.map(m -> gson.toJson(m)).map(session::textMessage));
        });


        map.put("/ship", session -> {
            session.receive()
                    .map(WebSocketMessage::getPayloadAsText)
                    .map(json -> gson.fromJson(json, ShipJson.class))
                    .subscribe(this::onShip,this::onError, this::onShipComplete);
            return session.send(shipProcessor.map(s -> gson.toJson(s)).map(session::textMessage));
        });

        final SimpleUrlHandlerMapping mapping = new SimpleUrlHandlerMapping();
        mapping.setOrder(1);
        mapping.setUrlMap(map);
        return mapping;
    }


    private void onMeteor(final MeteorJson m) {
        final Meteor avro = Meteor.newBuilder()
                .setBear(m.getBear())
                .setH(m.getH())
                .setId(m.getId())
                .setW(m.getW())
                .setX(m.getX())
                .setY(m.getY()).build();
        try(final Serializer<Meteor> serializer = meteorSerde.serializer()) {
            kafkaTemplate.send("Meteor", m.getZone().getBytes(), serializer.serialize("Meteor", avro));
        } catch (Exception err) {
            log.error("{}", err);
        }
    }

    private void onShip(final ShipJson ship) {
        if(ship.getDelete()) {
           kafkaTemplate.send("Ship", ship.getZone().getBytes(), null);
        } else {
            final Ship avro = Ship.newBuilder()
                    .setBear(ship.getBear())
                    .setH(ship.getH())
                    .setId(ship.getId())
                    .setW(ship.getW())
                    .setX(ship.getX())
                    .setY(ship.getY())
                    .build();
            try (final Serializer<Ship> serializer = shipSerde.serializer()) {
                kafkaTemplate.send("Ship", ship.getZone().getBytes(), serializer.serialize("Ship", avro));
            }
        }
    }

    private void onError(final Throwable err) {
        log.error("{}", err);
    }

    private void onShipComplete() {
        //Flush stores
    }

}
